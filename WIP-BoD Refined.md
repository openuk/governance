## Board of Directors
The Board of Directors represents the interests of OpenUK Supporters. The Board is the ultimate decision-making body of OpenUK except with respect to those matters reserved to Supporters. The business and affairs of OpenUK are managed by its Board. Although the Board typically delegates authority to members of OpenUK's Executive Team, the ultimate responsibility for OpenUK resides with the Board, which may not lawfully abdicate its duties.

## Core Responsibilities
OpenUK's Board of Directors core responsibilities consist of the following:

- Advising and counseling management regarding significant issues facing the Foundation;
- Evaluating and approving the Foundation’s strategic direction and initiatives, and monitoring their implementation and results;
- Monitoring the Foundation’s activities and financial condition;
- Overseeing the Foundation’s integrity and ethics, compliance with laws and financial integrity and reporting;
- Succession planning and management development;
- Assessing the performance of senior management and setting compensation, if applicable; and
- Understanding and assessing risks to the Foundation and monitoring the management of those risks.
- To fulfill their core responsibilities, Directors are expected to attend all meetings (except when special circumstances or pre-established material commitments dictate otherwise) and review materials in advance of the meetings.

## Director Duties and Responsibilities
Directors have an obligation to act in the best interests of OpenUK and its Supporters as a whole, including but not limited to acting with due care and owing a duty of loyalty to OpenUK.

A Director's duty of care, in general, means that they should be reasonably informed to participate in decisions of the Board and to do so prudently and in good faith. Important aspects of this duty include:

- Attendance at Meetings
	- You should attend all meetings of the Board of Directors (except when special circumstances or pre-established material commitments dictate otherwise). In order to do its job, the Board must act as a group, so your attendance at meetings is critical to ensure that all voices are heard.
- Exercising Independent Judgment
	- Directors serve as individuals, and the effectiveness of any vote or action they take as a member of the Board should not depend on whether it was authorised or directed by any other person or entity or was contrary to the instructions or interests of any other person or entity. Rather, you should weigh your votes on official action carefully, based upon your independent judgment, and you should abstain or dissent when you believe any action for which approval is sought is not in the best interests of OpenUK or its mission.
- Continuing to be Informed
	- In order for you to make prudent decisions for OpenUK, it is important that you remain informed. In furtherance of this, you should read all materials supplied to you and, to the extent that the information provided is inadequate, you should request additional information.
- Delegating Responsibilities
	- Since the Board of Directors does not perform the day-to-day operations of OpenUK, it must prudently select officers and managers and delegate these operations to the resulting Executive Team. Once delegated, however, the Board must oversee OpenUK’s Executive Team, via the appointed Chief Executive Officer, to ensure they are acting responsibly.
- Acting in OpenUK’s Best Interest
	- You should always act in good faith and in a manner you reasonably believe to be in OpenUK’s best interest, and with independent and informed judgment.

Your duty of loyalty means, generally, that you exercise your powers as a Director in the interest of OpenUK and not in your own interests or the interests of another entity or person. You may not use your position as a Director for personal advantage or to take an opportunity that is offered to OpenUK and use it personally. In some cases, you may have interests that conflict with those of OpenUK. You must be aware of the potential for such conflicts and disclose any potential conflict to the Board when this type of situation arises.

In addition, you should keep all matters and information relating to OpenUK in complete confidence until there has been general public disclosure by the appropriate spokesperson or the Board has decided as a whole that any director may disclose it, unless the information is already a matter of public record or common knowledge. All Directors are required to use reasonable care to protect and safeguard OpenUK’s confidential information.

Finally, you should familiarise yourself with all related policies that may have previously been adopted by the Board. If you believe that an activity undertaken by OpenUK is illegal, you should bring it to the attention of the full Board for investigation and inquiry.

## Reliance
In fulfilling your duties to OpenUK, you are entitled to rely in good faith upon the records of OpenUK and upon such information, opinions, reports or statements presented to OpenUK by any of OpenUK’s officers or employees, or committees of the Board of Directors, or by any other person as to matters you reasonably believe are within such other person’s professional or expert competence and who has been selected with reasonable care by or on behalf of OpenUK.

## Rights of Directors
To assist Directors in fulfilling their responsibilities as members of the Board of Directors, Directors have certain rights, including the following:

- Reasonable access to management (upon which more will be said below);
- Access to OpenUKs books and records, including financial statements;
- Adequate notice of meetings and the content thereof; and
- Minutes of meetings of the Board and its committees.

## Board Meeting Procedures

The Board will meet as frequently as necessary for the Directors to properly discharge their responsibilities. Meetings may be held on a regular schedule, and on occasion may be called on an interim basis as the need arises. Face-to-face meetings are ordinarily held several times a year, with the balance being held by telephonic or other electronic means of communication. The Board or its designee will establish the agenda for each Board meeting. Each Director is free to suggest the inclusion of agenda items, and each Director is free to raise at any Board meeting additional subjects that are not on the agenda for the meeting.

Information and data that are important to the Board’s understanding of the business to be conducted at meetings should be distributed in advance in writing to the Board with sufficient time for Directors to review and reflect on key issues. Directors should request such supplemental information as they believe appropriate before the Board meets. On those occasions on which the subject matter is too sensitive to put on paper, the presentation will be discussed at the meeting. Where there is no prior distribution of a presentation on a sensitive subject, it may be appropriate to advise each Director by email or telephone in advance of the meeting of the subject and the principal issues the Board will need to consider.

The Board may invite senior management and other guests to attend Board meetings.

## Access to Senior Management
Directors have access to OpenUK’s executive team. Directors should use judgment to be sure that this contact is not distracting to the business operation of OpenUK. Furthermore, the Board encourages the Chief Executive Officer to, from time to time, bring members of the executive team into Board meetings who: (a) can provide additional insight into the items being discussed because of personal involvement in these areas, and/or (b) represent managers with future potential that the senior management believes should be given exposure to the Board.

## Public Statements
A Director may not act in an official capacity or speak publicly on behalf of OpenUK unless empowered to do so by the CEO or as specifically empowered by the Board. Except where so empowered or authorised, a Director speaking publicly to OpenUK supporters and participants or in any other public forum must ensure that the Director’s statements are clearly identified as personal opinions and that the Director is not speaking on behalf of OpenUK in any official capacity or expressing the views or positions of OpenUK.

## Support of Board Decisions
A Director must accept and publicly support Board decisions. A Director is encouraged to be an ambassador of OpenUK and, subject to the Director’s confidentiality and other obligations, to promote the activities and actions of the Board with OpenUK supporters and participants and publicly. In doing so, a Director must stay faithful to the intent of the Board as expressed in its official statements, and should not reinterpret or re-characterize the Board’s actions to reflect the Director’s own view.

While a Director has the right and responsibility to exercise independent judgment and to express dissenting opinions during Board deliberations, a Director also has the obligation outside the Boardroom to respect and support decisions of the majority, even when the Director dissented from the majority view. A Director who does not support a Board decision may express the Director’s opposition within the Board in an appropriate manner, but must not take actions publicly or with respect to OpenUK supporters that have the purpose or result of undermining the decisions or actions of the Board. Acting otherwise may be in violation of a Director’s loyalty duties. Accordingly, a Director who intends to publicly oppose a Board action should resign the Director’s position on the Board before doing so.

```
Inspiration taken from:

https://github.com/nodejs/board/blob/master/Onboarding_Guide_for_Directors.md
```
