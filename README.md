# OpenUK Governance Framework
### https://openuk.uk/

---

This project articulates the [OpenUK](https://openuk.uk/) governance framework.

---

# Where UK’s Open communities meet and collaborate
OpenUK is a not for profit organisation committed to develop and sustain the United Kingdom's leadership in Open Technology -- open source software, open source hardware and open data.

OpenUK promotes businesses, projects and people, who use and contribute to Open Techology. As a professional body, we collaborate across these groups to creating a clear and meaningful voice for Open Communities, with particular focus on those in the United Kingdom. OpenUK influences government legal and policy frameworks and decisions to maintain and further substantiate the United Kingdom a leading market for Open Technology and business practice. We actively promote broad education and learning programmes to encourage skills development and knowledge sharing.

---

#### Please contact for further informtion:
**Sponsorship:**  amanda.brock@openuk.uk
**General:**  admin@openuk.uk