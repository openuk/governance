They've gone with this: https://docs.google.com/document/d/1SlWxRusE2mE1zekHDqDTb90ZenOqRXA_by4gnC4R18M/edit#


**Supporters**
Purpose of being a Supporter

OpenUK is as inclusive as possible and has a structure which does not exclude anyone from becoming a volunteer and participating in OpenUK, subject to complying with the Code of Respect and Competition Policy, but allowing individuals to pay to financially support the organisation and access additional benefits including voting rights by becoming a Supporter.

Financing from Partner companies is through Sponsorship and Donation. A business plan is  being developed by the Executive Team and shared with the Board in the November meeting.
Benefits of being a supporter / Why become a supporter?

Support the work of OpenUK
Be part of a movement to promote Open Technology
Connect with other members of the community, networking and mentorship
Discounts, free tickets or early access to events
Vote and nominate in Board elections
How to become a supporter and cost
Online signup for monthly membership
Agree to Declaration of alignment (affirmative statement of support for OpenUK vision), Code of Respect and Competition Policy
Membership is monthly and ongoing, renewed automatically until cancelled on one month’s notice
Monthly payments required, collected automatically via direct debit eg GoCardless, Stripe or PayPal recurring, until terminated
Sliding scale but not based on specific criteria, but rather on the individual’s choice, eg “Our goal is to keep it affordable enough that anyone can become a supporter, but we’d love to see people contributing more if they can”:
The suggested supporter contribution is £5 per month
Minimum: £2.50
£10
Other

Non-goals for Supporter Model
Corporate supporters - to preserve the neutrality of the board, supporters are individual people so that their votes are cast independently. Corporate entities can support financially through sponsorship or marketing initiatives, and this is separate to the governance process.
Corporate affiliations in your personal supporter role - people can have multiple project or corporate affiliations as well as personal opinions, and there is no sensible way or benefit to trying to track this for openUK
Changes in membership benefits based on tiers - we don’t have the systems or capacity to track multiple membership levels across events and activities

