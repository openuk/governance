**Board of Directors** 

Board Directors participate in the Board as individuals, not on behalf of their employer or any other external stakeholder.

The Board of Directors meets every two months via phone conference to discuss various topics pertaining to the regular activities of OpenUK with two of these meetings being strategy days in January and July.

Each Board member will be asked to participate in one of the 4 Board Committees, with an additional 4 meetings per year. The committees are Risk & Audit, Finance, Diversity & Respect, and Elections.

The current Board of Directors are the Founding Board and can be seen on the OpenUK web page. In future as well as the current Board being included in the web site, the history of the Board of Directors will list all past directors including the Founding Board.
Responsibilities of the Board of Directors

As a governance body:
- Ensures ‘good governance’
- Oversight of compliance with statutory, regulatory and fiscal requirements
- Strategic leadership
- Be a guiding hand and support CEO and Executive Team
- Accountability to supporters and external stakeholders 

As an individual Board Director:
- Evangelising Open Technology
- Nurturing stakeholders and stakeholder groups
- Acting as an ambassador for OpenUK and the Board externally
- Promoting the interests of OpenUK, with the benefits of diverse perspectives and experience working with Open Technologies
- Disclose any potential conflicts of interest, maintaining an accurate record of corporate affiliations and promptly declaring any changes

What are the benefits:
- A way to use your experience to support OpenUK
- Professional recognition
- It's fun!

How to become a Director?

- There are currently 12 Directors, which will be elected starting in 2021. Elections will be annual, for a two year term, so half of the seats will be up for election each year.
- As a one-off transition, we will find 6 volunteers from the current board to serve an additional year, so that the first elections in 2021 are for the other 6 seats.
- Supporters who joined over 6 months before the election are eligible to vote.
- Ballots will be cast online, in secret (although anonymised votes may be published for verification purposes) and counted using Scottish STV.
- No organization, corporation or similar entity, or any affiliate thereof, shall hold, directly or indirectly, more than 40% of the Board seats, regardless of election results. If corporate affiliations change, one member may need to step down.
- The Directors may, at their discretion, appoint additional Directors post-election to ensure representation, expertise, etc.

Nomination Process

UK residency in the preceding 12 months
- Nominees must be existing supporters
- Current Directors may stand for re-election once, before they must take a 1-year break
- Corporate or other organisational affiliations must be disclosed as part of your nomination
- Nominations must be seconded by three existing supporters who are eligible to vote. Seconders are attesting to the nomination being accurate and the candidate meeting these criteria.

Non-goals for Directors

- Directors are not involved in executive or operational decision making. There is no requirement to volunteer in other parts of the organisation, but equally it is not prohibited in an individual capacity.
- “Council of elders” to protect against entryism - general consensus of the group was that the 6-month requirement for supporters to second nominations or votes, combined with the board being elected in staggered 2 year terms, was an adequate set of safeguards.
- Corporate independence - Essential for fund-raising and sponsorship that OpenUK and the board is comprised of individuals who primarily promote the interests of OpenUK. Directors can and should draw on their professional experience, but are not merely directly representing a corporate interest in their board seat. A limit of board members from the same affiliates is imposed for this reason.

